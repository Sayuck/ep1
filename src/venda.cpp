#include <string>
#include <iostream>
#include "venda.hpp"
#include "funcoes.hpp"
#include <vector>
#include <sstream>
#include <cstdlib>
#include <iterator>

using namespace std;

Venda::Venda()
{
    recebeClientes();
    recebeProdutos();

    do
    {

        cout << "Escolha a Função:\n1-Venda\n2-Cadastrar Produtos\n3-Repor Estoque\n4-Cadastrar Cliente\n5-Modo Recomendação\n0-sair\n"
             << endl;
        escolha = validaInt();

        switch (escolha)
        {
        case 1:

            int aux;

            cout << "Digite o CPF do Cliente" << endl;
            cpf = validaLong();

            aux = consultaCPF(cpf);
            if (aux == -1)
            {

                break;
            }
            else
            {
                cout << "Cliente: " << todosClientes[aux].getNome() << endl;
            }

            cout << "Produtos Disponíveis\n"
                 << endl;

            for (int cont = 0; cont < (int)todosProdutos.size(); cont++)
            {
                cout.precision(2);
                cout << "Produto: " << todosProdutos[cont].getNome() << "  Código: " << todosProdutos[cont].getCodigo() << "  Preço Unitário: " << fixed << todosProdutos[cont].getpreco() << endl;
            }

            cout << "\nEscolha um Produto Digitando Seu Código" << endl;

            carrinho(aux);

            break;

        case 2:

            cadastrarProdutos();

            break;

        case 3:
            atualizaQuantidade();

            break;

        case 4:
            cadastrarUsuario();

            break;
        case 5:
            cout << "Recurso Indisponível" << endl;
            break;
        case 0:
            break;
        default:
            cout << "Opção Inválida!" << endl;
        }

    } while (escolha != 0);
    salvaProdutosFinal();
    salvaClientesFinal();
}
Venda::~Venda()
{
}
void Venda::salvaProdutosFinal()
{
    salvaProdutos.open("./inc/Estoque.txt", ios::out);
    if (!salvaProdutos.is_open())
    {
        cout << "Erro ao abrir arquivo" << endl;
        return;
    }
    while (todosProdutos.size())
    {
        salvaProdutos << todosProdutos[todosProdutos.size() - 1].getNome() << endl;
        salvaProdutos << todosProdutos[todosProdutos.size() - 1].getpreco() << endl;
        salvaProdutos << todosProdutos[todosProdutos.size() - 1].getCodigo() << endl;
        salvaProdutos << todosProdutos[todosProdutos.size() - 1].getQuantidade() << endl;
        salvaProdutos << todosProdutos[todosProdutos.size() - 1].getCategoria() << endl;
        todosProdutos.pop_back();
    }
    salvaProdutos.close();
}
void Venda::cadastrarProdutos()
{
    int n;
    do
    {
        cout << "Digite o Nome do Produto: ";
        getline(cin, produto);
        cout << "\nProduto Inserido: " << produto << "\n\nConfirma Nome?\n\nDIGITE 0 - Para Confirmar  Outro Valor - Redefinir: " << endl;
        n = validaInt();
    } while (n != 0);
    do
    {
        cout << "Insira o Valor Unitário do Produto: ";
        preco = validaFloat();
        cout << "\nValor Inserido: " << preco << "\n\nConfirma Valor?\n\nDIGITE 0 - Para Confirmar  Outro Valor - Redefinir: " << endl;
        n = validaInt();
    } while (n != 0);
    do
    {
        cout << "Digite o Código do Produto: ";
        codigo = validaInt();
        int verificaCodigo = verificaCodigoProduto(codigo);
        if (verificaCodigo == -1)
        {
            cout << "\nCódigo Inserido: " << codigo << "\n\nConfirma Código?\n\nDIGITE 0 - Para Confirmar  Outro Valor - Redefinir: " << endl;
            n = validaInt();
        }
        else
        {
            n = 1;
        }
    } while (n != 0);
    do
    {
        cout << "Digite a Quantidade de Produtos no Estoque: ";
        quantidade = validaInt();
        cout << "\nQuantidade Inserida no Estoque: " << quantidade << "\n\nConfirma Quantidade?\n\nDIGITE 0 - Para Confirmar  Outro Valor - Redefinir: " << endl;
        n = validaInt();
    } while (n != 0);
    Estoque estoque = Estoque(produto, preco, codigo, quantidade);
    todosProdutos.push_back(estoque);
    cout << todosProdutos[todosProdutos.size() - 1].getNome() << " Cadastrado com sucesso" << endl;
}

void Venda::salvaClientesFinal()
{
    salvaClientes.open("./inc/Clientes.txt", ios::out);
    if (!salvaClientes.is_open())
    {
        cout << "Erro ao abrir aquivo" << endl;
    }
    while (todosClientes.size())
    {
        Cliente auxiliar;
        auxiliar = Cliente();
        auxiliar.setNome(todosClientes[todosClientes.size() - 1].getNome());
        auxiliar.setEmail(todosClientes[todosClientes.size() - 1].getEmail());
        auxiliar.setCpf(todosClientes[todosClientes.size() - 1].getCpf());
        auxiliar.setTelefone(todosClientes[todosClientes.size() - 1].getTelefone());
        auxiliar.setSocio(todosClientes[todosClientes.size() - 1].isSocio());

        salvaClientes << auxiliar.getNome() << endl;
        salvaClientes << auxiliar.getEmail() << endl;
        salvaClientes << auxiliar.getCpf() << endl;
        salvaClientes << auxiliar.getTelefone() << endl;
        salvaClientes << auxiliar.isSocio() << endl;

        todosClientes.pop_back();
    }
    salvaClientes.close();
}

void Venda::salvaHistoricoFinal()
{
}

void Venda::cadastrarUsuario()
{
    int n;
    do
    {
        cout << "\nDigite o Nome do Cliente: ";
        //cin.clear();
        //cin.ignore();
        getline(cin, nome);
        cout << "\nNome Inserido: " << nome << " \n\nConfirma Nome?\n DIGITE 0 - Para Confirmar  Outro Valor - Redefinir: " << endl;
        n = validaInt();
    } while (n != 0);

    do
    {
        cout << "\nDigite o email: ";
        //cin.ignore();
        getline(cin, email);
        cout << "\nEmail Inserido: " << email << "\n\nConfirma Email?\n DIGITE 0 - Para Confirmar  Outro Valor - Redefinir: ";
        n = validaInt();
    } while (n != 0);

    do
    {
        cout << "\nDigite o número do CPF, APENAS NUMEROS : ";
        cpf = validaLong();
        cout << "CPF Inserido: " << cpf << "\n\nConfirma CPF?\n DIGITE 0 - Para Confirmar  Outro Valor - Redefinir: ";
        n = validaInt();
    } while (n != 0);

    do
    {
        cout << "\nDigite o numero do telefone: ";
        telefone = validaInt();
        cout << "\nTelefone Inserido: " << telefone << "\n\nConfirma Telefone?\n DIGITE 0 - Para Confirmar  Outro Valor - Redefinir: ";
        n = validaInt();
    } while (n != 0);

    do
    {
        cout << "\nCliente quer ser sócio? \n\nEscolha 0 para não outro valor para sim" << endl;
        sociedade = validaInt();
        if (sociedade == 0)
        {
            socio = false;
        }
        else
        {
            socio = true;
        }
        cout << "\nSociedade Definida como: " << (socio == true ? "Sócio" : "Não Sócio");
        cout << "\nConfirma Sociedade?\n DIGITE 0 - Para Confirmar  Outro Valor - Redefinir: ";
        n = validaInt();
    } while (n != 0);
    Cliente cliente = Cliente(nome, email, cpf, telefone, socio);
    todosClientes.push_back(cliente);
    cout << todosClientes[todosClientes.size() - 1].getNome() << " Cadastrado com sucesso" << endl;
}

void Venda::recebeProdutos()
{
    string product;
    listaProdutos.open("./inc/Estoque.txt", ios::in | ios::app);
    Estoque estoque = Estoque();
    while (listaProdutos)
    {
        getline(listaProdutos, product);
        estoque.setNome(product);
        getline(listaProdutos, product);
        estoque.setPreco(atof(product.c_str()));
        getline(listaProdutos, product);
        estoque.setCodigo(atoi(product.c_str()));
        getline(listaProdutos, product);
        estoque.setQuantidade(atoi(product.c_str()));
        getline(listaProdutos, product);
        estoque.setCategoria(product);
        todosProdutos.push_back(estoque);
    }
    todosProdutos.pop_back();
}
void Venda::recebeClientes()
{
    string nometeste;
    listaClientes.open("./inc/Clientes.txt", ios::in | ios::app);
    Cliente cliente = Cliente();
    while (listaClientes)
    {
        getline(listaClientes, nometeste);
        cliente.setNome(nometeste);
        getline(listaClientes, nometeste);
        cliente.setEmail(nometeste);
        getline(listaClientes, nometeste);
        cliente.setCpf(atoi(nometeste.c_str()));
        getline(listaClientes, nometeste);
        cliente.setTelefone(atoi(nometeste.c_str()));
        getline(listaClientes, nometeste);
        cliente.setSocio(atoi(nometeste.c_str()));
        todosClientes.push_back(cliente);
    }
    todosClientes.pop_back();
}
int Venda::consultaCPF(long int cpf)
{
    unsigned int i;
    for (i = 0; i < todosClientes.size(); i++)
    {
        if (cpf == todosClientes[i].getCpf())
        {
            return i;
            //break;
        }
    }
    cout << "Cliente Não cadastrado" << endl;
    cout << "Deseja Cadastrar Novo Cliente?\nEscolha \n\n0 - NÃO \n1 - SIM" << endl;
    int n;
    do
    {
        n = validaInt();
        switch (n)
        {
        case 1:
            cadastrarUsuario();
            cout << "Digite o CPF do Cliente" << endl;
            cpf = validaLong();
            i = consultaCPF(cpf);
            return i;
            break;
        case 0:
            break;
        default:
            cout << "escolha inválida" << endl;
            break;
        }
    } while (n != 0);
    return -1;
}
void Venda::carrinho(int aux)
{
    unsigned int i, cont;
    int quant;
    float valorTotal, valorRecebido, troco;
    codigo = validaInt();
    for (cont = 0; cont < todosProdutos.size(); cont++)
    {
        if (codigo == todosProdutos[cont].getCodigo())
        {

            listaCarrinho.push_back(todosProdutos[cont]);
            cout << todosProdutos[cont].getNome() << " Entrou no Carrinho, Insira a Quantidade desse Produto: " << endl;
            quant = validaInt();
            if (quant == 0)
            {
                cout << "Retirando Produto do Carrinho..." << endl;
                listaCarrinho.pop_back();
                break;
            }
            else
            {
                if (todosProdutos[cont].getQuantidade() < quant)
                {
                    cout << "Produto em Quantidade Insuficiente, Cancelando venda..." << endl;
                    listaCarrinho.pop_back();
                    break;
                }
                else
                {
                    listaCarrinho[listaCarrinho.size() - 1].setQuantidade(quant);
                    cout << listaCarrinho[listaCarrinho.size() - 1].getNome() << " Quantidade: " << listaCarrinho[listaCarrinho.size() - 1].getQuantidade() << endl;
                }
                break;
            }
        }
    }
    if (cont == todosProdutos.size())
        cout << "Produto Não Cadastrado" << endl;

    do
    {
        cout << "Escolha a Função:\n1 - Inserir Mais Produtos\n2 - Listar Produtos no Carrinho\n3 - Seguir Para o Caixa\n0 - Cancelar Compra" << endl;
        i = validaInt();
        switch (i)
        {
        case 1:
            cout << "Insira Outro Código de Produto: ";

            codigo = validaInt();

            for (cont = 0; cont < todosProdutos.size(); cont++)
            {
                if (codigo == todosProdutos[cont].getCodigo())
                {
                    for (int a = 0; a < (int)listaCarrinho.size(); a++)
                    {
                        if (codigo == listaCarrinho[a].getCodigo())
                        {
                            cout << "Produto já se Encontra no Carrinho, Impossivel Inserir Novamente\n Compra Cancelada!" << endl;
                            listaCarrinho.clear();
                            return;
                        }
                    }
                    listaCarrinho.push_back(todosProdutos[cont]);
                    cout << todosProdutos[cont].getNome() << " Entrou no Carrinho, Insira a Quantidade desse Produto: " << endl;
                    quant = validaInt();
                    if (quant == 0)
                    {
                        cout << "Retirando Produto do Carrinho..." << endl;
                        listaCarrinho.pop_back();
                        break;
                    }
                    else
                    {
                        if (todosProdutos[cont].getQuantidade() < quant)
                        {
                            cout << "Produto em Quantidade Insuficiente, Cancelando venda..." << endl;
                            listaCarrinho.pop_back();
                            break;
                        }
                        else
                        {
                            listaCarrinho[listaCarrinho.size() - 1].setQuantidade(quant);
                            cout << listaCarrinho[listaCarrinho.size() - 1].getNome() << " Quantidade: " << listaCarrinho[listaCarrinho.size() - 1].getQuantidade() << endl;
                        }
                        break;
                    }
                }
            }
            if (cont == todosProdutos.size())
                cout << "ERRO! Produto Não Cadastrado" << endl;
            break;
        case 2:
            valorTotal = 0;
            cout << "Produtos no Carrinho: " << endl;
            cout << "==========================================================" << endl;
            for (cont = 0; cont < listaCarrinho.size(); cont++)
            {
                cout.precision(2);
                cout << listaCarrinho[cont].getNome() << " Quantidade: " << listaCarrinho[cont].getQuantidade() << " Valor: " << fixed << listaCarrinho[cont].getpreco() * listaCarrinho[cont].getQuantidade() << endl;
                valorTotal = valorTotal + listaCarrinho[cont].getpreco() * listaCarrinho[cont].getQuantidade();
            }
            cout.precision(2);
            cout << "Valor Total dos Produtos: " << fixed << valorTotal << endl;
            if (todosClientes[aux].isSocio())
            {
                valorTotal = valorTotal * 0.85;
                cout.precision(2);
                cout << "Valor Total dos Produtos Com Desconto de Sócio: " << fixed << valorTotal << endl;
            }
            cout << "==========================================================" << endl;
            break;
        case 3:
            valorTotal = 0;
            cout << "Produtos // Valor:" << endl;
            cout << "==========================================================" << endl;
            for (cont = 0; cont < listaCarrinho.size(); cont++)
            {
                cout.precision(2);
                cout << listaCarrinho[cont].getNome() << " Quantidade: " << listaCarrinho[cont].getQuantidade() << " Valor: " << fixed << listaCarrinho[cont].getpreco() * listaCarrinho[cont].getQuantidade() << endl;
                valorTotal = valorTotal + listaCarrinho[cont].getpreco() * listaCarrinho[cont].getQuantidade();
            }
            cout.precision(2);
            cout << "Valor Total dos Produtos: " << fixed << valorTotal << endl;
            if (todosClientes[aux].isSocio())
            {
                valorTotal = valorTotal * 0.85;
                cout.precision(2);
                cout << "Valor Total dos Produtos Com Desconto de Sócio: " << fixed << valorTotal << endl;
            }
            cout << "==========================================================" << endl;
            cout << "Valor Recebido: ";
            valorRecebido = validaFloat();
            if (valorRecebido < valorTotal)
            {
                cout << "Valor Insuficiente Para a Compra!";
                break;
            }
            else
            {

                troco = valorRecebido - valorTotal;
                cout.precision(2);
                cout << "Troco: " << fixed << troco << "\nObrigado por Comprar Conosco, Volte Sempre!\n"
                     << endl;

                while (listaCarrinho.size())
                {

                    for (unsigned int a = 0; a < todosProdutos.size(); a++)
                    {
                        if (listaCarrinho[listaCarrinho.size() - 1].getCodigo() == todosProdutos[a].getCodigo())
                        {
                            todosProdutos[a].setQuantidade((todosProdutos[a].getQuantidade()) - (listaCarrinho[listaCarrinho.size() - 1].getQuantidade()));
                        }
                    }
                    listaCarrinho.pop_back();
                }
                return;
            }
            break;
        case 0:

            listaCarrinho.clear();
            cout << "Compra Cancelada!" << endl;

            break;

        default:
            cout << "Opção Inválida" << endl;
            break;
        }

    } while (i != 0);
}

void Venda::atualizaQuantidade()
{
    int cod;
    unsigned int i;
    for (int cont = 0; cont < (int)todosProdutos.size(); cont++)
    {
        cout << "Produto: " << todosProdutos[cont].getNome() << "  Código: " << todosProdutos[cont].getCodigo() << "\n"
             << endl;
    }
    cout << "Digite o código do Produto: ";
    cod = validaInt();
    for (i = 0; i < todosProdutos.size(); i++)
    {
        if (cod == todosProdutos[i].getCodigo())
        {
            cout << "Produto: " << todosProdutos[i].getNome() << " Quantidade Atual no Estoque: " << todosProdutos[i].getQuantidade() << endl;
            cout << "Quantidade que Deseja Adicionar ao Estoque: ";
            int qtd = validaInt();
            if (qtd < 0)
            {
                cout << "Não permitida retirada de produtos diretamente do estoque\n" << endl;
                return;
            }
            else
            {

                todosProdutos[i].setQuantidade(todosProdutos[i].getQuantidade() + qtd);
                cout << "Produto: " << todosProdutos[i].getNome() << " Quantidade Atual no Estoque: " << todosProdutos[i].getQuantidade() << endl;
                break;
            }
        }
    }
    if (i == todosProdutos.size())
        cout << "Produto Não Cadastrado!" << endl;
}

int Venda::verificaCodigoProduto(int cod)
{
    int i;
    for (i = 0; i < (int)todosProdutos.size(); i++)
    {
        if (cod == todosProdutos[i].getCodigo())
        {
            cout << "Código já Cadastrado, Digite Outro Para o Produto" << endl;
            return i;
        }
    }
    return -1;
}