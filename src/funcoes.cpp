#include "funcoes.hpp"
#include <iostream>
#include <iomanip>
using namespace std;

int validaInt()
{
    while (true)
    {
        int num;
        cin >> num;
        if (cin.fail())
        {

            cout << "Erro entre com um número: ";
            cin.clear();
            cin.ignore(INT16_MAX, '\n');
        }
        else
        {
            cin.ignore(INT16_MAX, '\n');
            return num;
        }
    }
}
float validaFloat()
{
    while (true)
    {
        float num;
        cin >> num;
        if (cin.fail())
        {

            cout << "Erro entre com um número válido: ";
            cin.clear();
            cin.ignore(3215465.1546546, '\n');
        }
        else
        {
            cin.ignore(3215465.1546546, '\n');
            return num;
        }
    }
}
long int validaLong()
{
    while (true)
    {
        long int num;
        cin >> num;
        if (cin.fail())
        {

            cout << "Erro entre com um número: ";
            cin.clear();
            cin.ignore(INT32_MAX, '\n');
        }
        else
        {
            cin.ignore(INT32_MAX, '\n');
            return num;
        }
    }
}
