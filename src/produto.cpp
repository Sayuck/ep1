#include <string>
#include <iostream>
#include "produto.hpp"

using namespace std;

Produto::Produto()
{
}
Produto::Produto(string nome, float preco, int codigo)
{
    setNome(nome);
    setPreco(preco);
    setCodigo(codigo);
}
Produto::~Produto()
{
}
void Produto::setNome(string nome)
{
    this->nome = nome;
}
string Produto::getNome()
{
    return nome;
}
void Produto::setPreco(float preco)
{
    this->preco = preco;
}
float Produto::getpreco()
{
    return preco;
}
void Produto::setCodigo(int codigo)
{
    this->codigo = codigo;
}
int Produto::getCodigo()
{
    return codigo;
}