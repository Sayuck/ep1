#include "estoque.hpp"

Estoque::Estoque()
{
}
Estoque::Estoque(string nome, float preco, int codigo, int quantidade)
{
    setNome(nome);
    setPreco(preco);
    setCodigo(codigo);
    setQuantidade(quantidade);
    int escolha;
    string categorias, aux;
    do
    {

        cout << "Insira a categoria do produto" << endl;
        cin.clear();
        //cin.ignore();
        getline(cin, categorias);
        //cin >> categorias;
        transform(categorias.begin(), categorias.end(), categorias.begin(), ::toupper);
        cout << "Digite 0 para sair ou outro valor para inserir mais um categoria para o produto" << endl;
        aux = categorias + " " + aux;
        escolha = validaInt();
    } while (escolha != 0);

    setCategoria(aux);
}

Estoque::~Estoque()
{
}
void Estoque::setQuantidade(int quantidade)
{
    this->quantidade = quantidade;
}
int Estoque::getQuantidade()
{
    return quantidade;
}
void Estoque::setCategoria(string categoria)
{
    this->categoria = categoria;
}
string Estoque::getCategoria()
{
    return categoria;
}
