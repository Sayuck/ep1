#include <iostream>
#include "Cliente.hpp"

Cliente::Cliente(string nome, string email, long int cpf, int telefone, bool socio)
{
    setNome(nome);
    setEmail(email);
    setCpf(cpf);
    setTelefone(telefone);
    setSocio(socio);
    //cout<< " Cliente criado com sucesso "<<this->nome<<" Possui cadastro agora"<< endl;
}
Cliente::Cliente()
{
}
Cliente::~Cliente()
{
    //cout << "cliente deletado" << endl;
}
string Cliente::getNome()
{
    return nome;
}
void Cliente::setNome(string nome)
{
    this->nome = nome;
}
string Cliente::getEmail()
{
    return email;
}
void Cliente::setEmail(string email)
{
    this->email = email;
}
int Cliente::getTelefone()
{
    return telefone;
}
void Cliente::setTelefone(int telefone)
{
    this->telefone = telefone;
}
int Cliente::getCpf()
{
    return cpf;
}
void Cliente::setCpf(long int cpf)
{
    this->cpf = cpf;
}
bool Cliente::isSocio()
{
    return socio;
}
void Cliente::setSocio(bool socio)
{
    this->socio = socio;
}