#ifndef FUNCOES_HPP
#define FUNCOES_HPP
#include "Cliente.hpp"
#include "estoque.hpp"
#include <iostream>

int validaInt();
long int validaLong();
float validaFloat();

#endif