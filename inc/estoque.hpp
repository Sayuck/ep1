#ifndef ESTOQUE_HPP
#define ESTOQUE_HPP
#include "produto.hpp"
#include <iostream>
#include <algorithm>
#include <vector>
#include "funcoes.hpp"
using namespace std;

class Estoque : public Produto
{
private:
    int quantidade;
    string categoria;

public:
    Estoque();
    Estoque(string nome, float preco, int codigo, int quantidade);
    ~Estoque();
    void setQuantidade(int quantidade);
    int getQuantidade();
    void setCategoria(string categoria);
    string getCategoria();
};
#endif
