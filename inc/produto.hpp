#ifndef PRODUTO_HPP
#define PRODUTO_HPP
#include <string>
using namespace std;
class Produto
{
private:
    string nome;
    float preco;
    int codigo;

public:
    Produto();
    Produto(string nome, float preco, int codigo);
    ~Produto();
    void setNome(string nome);
    string getNome();
    void setPreco(float preco);
    float getpreco();
    void setCodigo(int codigo);
    int getCodigo();
};

#endif