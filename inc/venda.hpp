#ifndef VENDA_HPP
#define VENDA_HPP
#include <fstream>
#include "Cliente.hpp"
#include "estoque.hpp"
#include <string>
#include <iostream>
#include <vector>

using namespace std;

class Venda
{
private:
    ifstream listaProdutos;
    ifstream listaClientes;
    ifstream historicoCompras;
    ofstream salvaProdutos;
    ofstream salvaClientes;
    ofstream salvaHistorico;
    string produto, nome, email;
    int escolha, telefone, sociedade, codigo, quantidade;
    float preco;
    long int cpf;
    bool socio;
    vector<Cliente> todosClientes;
    vector<Estoque> todosProdutos;
    vector<Estoque> listaCarrinho;

public:
    Venda();
    ~Venda();
    void recebeClientes();
    void recebeProdutos();
    void salvaProdutosFinal();
    void salvaClientesFinal();
    void salvaHistoricoFinal();
    void cadastrarUsuario();
    void cadastrarProdutos();
    int consultaCPF(long int cpf);
    void carrinho(int aux);
    void atualizaQuantidade();
    int verificaCodigoProduto(int cod);
};
#endif