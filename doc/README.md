# EP1 - OO 2019.2(UnB-Gama)

#### Roberto Martins da Nóbrega Matrícula: 14/0065547

### Apresentação:
O software foi designado para operação de um funcionário do estabelecimento, logo apenas funcionários verão os menus a seguir.


O software possui 4 modos funcionais:


## 1. Modo Venda: 

Aqui você insere o CPF do cliente para poder prosseguir com a compra, caso o CPF não esteja cadastrado será ofertado o cadastro de usuário, condição necessária para que continue a venda.

 1.1 Após a inserção do CPF ja cadastrado no banco de dados da loja aparecerá o nome do cliente que teve o CPF inserido e uma lista com os produtos disponíveis na loja com seu preço e código, código esse necessário para continuar na compra, inserindo-o recebe-se a mensagem que o produto em questão está no carrinho e precisa inserir a quantidade que deseja-se comprar.

 1.2 Com a inserção do primeiro produto entra-se no segundo menu da venda onde será possível 1- Inserir mais produtos 2- Listar produtos do carrinho 3- Seguir para o caixa ou 0 - para cancelar a compra.

## 2. Modo Cadastrar Produtos: 
Aqui você insere o nome do produto, seu preço unitário, o código do produto, a quantidade desse produto no estoque e as suas categorias, todos serão confirmados com mensagem mostrando o que foi digitado e se deseja confirmar exceto na categoria que a confirmação será para inserir novas categorias para o produto em questão.

## 3. Modo Repor Estoque: 
Aqui você insere o codigo do produto e será apresentado o nome do respectivo produto e quantos ainda restam no estoque, em seguida pedirá para inserir a quantidade que deseja adicionar ao estoque, após a inserção será apresentada novamente o nome do produto e sua quantidade atual.

## 4. Modo Cadastrar Clente: 
Aqui se insere o Nome, Email, CPF (importante para realizar vendas para o cliente), telefone e se o cliente deseja se tornar sócio do estabelecimento, opção disponível apenas ao realizar cadastro.

## 5. Modo Recomendação: 
Indisponível no momento.

